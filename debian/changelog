libhtml-html5-parser-perl (0.992-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 28 Aug 2022 14:15:57 +0100

libhtml-html5-parser-perl (0.992-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)
    + fix encoding issues;
      closes: bug#750946

  [ Salvatore Bonaccorso ]
  * update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * use MetaCPAN URIs (not search.cpan.org or www.cpan.org)
  * use secure GitHub URIs

  [ Debian Janitor ]
  * set upstream metadata fields:
    Bug-Database Repository

  [ Jonas Smedegaard ]
  * simplify rules;
    stop build-depend on dh-buildinfo cdbs
  * annotate test-only build-dependencies
  * use debhelper compatibility level 13 (not 9);
    build-depend on debhelper-compat (not debhelper)
  * set Rules-Requires-Root: no
  * enable autopkgtest
  * update watch file:
    + use file format 4
    + mention gbp --uscan in usage comment
    + use substitution strings
  * simplify source helper script copyright-check
  * update copyright info:
    + stop track no longer embedded code
    + sort License sections alphabetically
    + update coverage
    + list GitHub issue tracker as preferred upstream contact
  * use semantic newlines in long description and copyright fields
  * build-depend on libtest-requires-perl
  * declare compliance with Debian Policy 4.6.0

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 08 Sep 2021 23:44:32 +0200

libhtml-html5-parser-perl (0.301-2) unstable; urgency=medium

  [ Jonas Smedegaard ]
  * Build-depend on libmoo-perl libwww-perl: Optionally needed for
    testsuite.
  * Fix recommend libwww-perl.
  * Modernize Vcs-* fields:
    + Use https protocol.
    + Use git (not gitweb) in path.
  * Declare compliance with Debian Policy 4.1.0.
  * Use debhelper compatibility level 9 (not 8).
  * Update git-buildpackage config: Filter any .git* files.
  * Update watch file:
    + Use format version 4.
    + Watch MetaCPAN URL.
    + Mention gbp --uscan in usage comment.
    + Tighten version regex.
  * Modernize cdbs:
    + Drop upstream-tarball hints: Use gbp import-orig --uscan.
    + Do copyright-check in maintainer script (not during build).
      Stop build-depend on devscripts.
  * Relax to (build-)depend unversioned on libxml-libxml-perl: Needed
    version satisfied even in oldstable.
  * Build-depend unversioned on perl (not (build-)depend explicitly on
    (recent perl or) libhttp-tiny-perl): Needed version included with
    perl even in oldstable.
  * Update copyright info:
    + Use https protocol in file format URL.
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Extend coverage for myself.
  * Add lintian overrides regarding License-Reference.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 01 Sep 2017 01:28:06 +0200

libhtml-html5-parser-perl (0.301-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Use canonical hostname (anonscm.debian.org) in Vcs-Git URI.

  [ Jonas Smedegaard ]
  * Add README.source emphasizing control.in file as *not* a
    show-stopper for contributions, referring to wiki page for details.
  * Bump standards-version to 3.9.4.
  * Drop patch 1001: Build problem turned out to be (and fixed by now)
    in CDBS.
  * Add git URL as alternate source.
  * Stop tracking md5sum of upstream tarball.
  * List upstream issue tracker as preferred contact.
  * Update Homepage to use metacpan.com, to match upstream hint.
  * Bump packaging license to GPL-3+, and extend copyrigt coverage for
    myself to include recent years.
  * Update copyright coverage for additional convenience code copies.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 15 Jul 2013 21:33:38 +0200

libhtml-html5-parser-perl (0.208-1) unstable; urgency=low

  [ upstream ]
  * New upstream release.
    + Bugfix: If two <html> tags were in the same file, attributes on
      the second <html> element could cause crashes.
    + Bugfix: Minor fix re LWP-independence.


  [ Jonas Smedegaard ]
  * Recommend libwww-perl.
  * Unfuzz patch.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 11 Dec 2012 01:11:47 +0100

libhtml-html5-parser-perl (0.206-2) unstable; urgency=low

  * Bump debhelper compatibility level to 8.
  * Update package relations:
    + Relax to (build-)depend unversioned on cdbs: Needed version
      satisfied in stable, and oldstable no longer supported.
  * Update copyright file:
    + Fix use pseudo-license-in-comment and -comment-in-license fields:
      File format 1.0 mandates License field to either be single-line or
      include all licensing info.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 07 Aug 2012 16:38:44 +0200

libhtml-html5-parser-perl (0.206-1) experimental; urgency=low

  * New upstream release.

  * (Build-)depend on recent perl or libttp-tiny-perl (not libwww-perl),
    and liburi-perl.
  * Update copyright file to cover yet another convenience copy of
    external Perl module.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 30 Jun 2012 21:05:10 +0200

libhtml-html5-parser-perl (0.200-1) experimental; urgency=low

  * New upstream release.

  * Update inclusion of example files.
  * (Build-)depend on libio-html-perl and libtry-tiny-perl (not
    liberror-perl or libhtml-encoding-perl).
  * Update copyright file:
    + Improve Files sections for convenience copies of external Perl
      modules.
    + Quote license string in GPL comment.
  * Add patch 1001 to avoid auto-installing dependencies during build.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 14 Jun 2012 18:57:17 +0200

libhtml-html5-parser-perl (0.110-1) unstable; urgency=low

  * New upstream release.
  * Use anonscm.debian.org URL in Vcs-Browser field.
  * Update package relations:
    + Newline-delimit dependencies.
    + Suggest (not yet packaged) libxml-libxml-devel-setlinenumber-perl.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 25 Mar 2012 14:13:59 +0200

libhtml-html5-parser-perl (0.109-1) unstable; urgency=low

  * New upstream release.

  [Jonas Smedegaard]
  * Fix use by-module URL for get-orig-source rule.
  * Update copyright file:
    + Improve coverage of convenience code copies below inc/.
    + Fix double-indent in Copyright fields as per Policy §5.6.13.
    + Quote license strings in comments.
  * List Florian as copyright holder in debian/rules, and extend
    copyright years for myself.
  * Update package relations:
    + Relax build-dependency on cdbs (needlessly tight).
    + Relax build-depend unversioned on debhelper and devscripts (needed
      versions satisfied even in oldstable).


  [ Florian Schlichting ]
  * Tighten dependency on XML::LibXML to at least version 1.93.
  * Bump Standards-Version to 3.9.3.
  * Update copyright file:
    + Bump copyright format to 1.0.
    + Adjust copyright year for Scalar::Util version 1.23.
    + Add files paragraph for html5lib test data.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 19 Mar 2012 22:54:41 +0100

libhtml-html5-parser-perl (0.108-1) unstable; urgency=low

  * Imported Upstream version 0.108.
  * Bumped years of copyright.
  * Included a separate copyright stanza for M::P::Dist::RDF.
  * Don't install sample scripts into /usr/bin but into examples/.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Thu, 19 Jan 2012 21:20:09 +0100

libhtml-html5-parser-perl (0.107-1) unstable; urgency=low

  [ Florian Schlichting ]
  * Imported Upstream version 0.107.
  * Dropped 2001_disable_verification.patch, modified file was removed.
  * Bumped copyright years, adjusted DEP-5 headers and added/deleted stanzas
    for new and removed files.
  * Added dependency on libhtml-html5-entities-perl, dropped dependency on
    libmodule-signature-perl.
  * Added myself to uploaders and copyright.

  [ Jonas Smedegaard ]
  * Update copyright file: Improve references for convenience copy of
    Module::Install.

  [ gregor herrmann ]
  * Remove debian/source/local-options; abort-on-upstream-changes and
    unapply-patches are default in dpkg-source since 1.16.1.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Sun, 18 Dec 2011 23:18:51 +0100

libhtml-html5-parser-perl (0.103-2) unstable; urgency=low

  * Improve package relations:
    + Stop declaring packages part of core Perl even in oldstable:
      - libtest-simple-perl
  * Update copyright file:
    + Rewrite using draft 174 of DEP-5 format.
    + Rewrap license fields at 72 chars, and shorten comments.
  * Bump policy compliance to standards-version 3.9.2.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 08 May 2011 20:27:12 +0200

libhtml-html5-parser-perl (0.103-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#616516.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 05 Mar 2011 08:04:44 +0100
